'use strict';

module.exports = function createDownloader(crawler, writer) {
  async function download(albumUrl, destination) {
    const song = await crawler.getSong(albumUrl);
    await writer.write(song, destination);
    return {
      result: 'ok'
    };
  }

  return { download };
}