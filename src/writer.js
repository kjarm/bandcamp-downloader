'use strict';

const { join } = require('path');

module.exports = function createWriter(https, fs) {
  function songFileDestination(song, destination) {
    return join(destination, `${song.normalizedTitle}.mp3`);
  }
  async function write(song, destination) {
    const file = fs.createWriteStream(songFileDestination(song, destination));
    return new Promise((resolve, reject) => {
      https.get(song.url, res => {
        res.pipe(file);
        res.on('error', reject);
        file.on('error', reject);
        file.on('finish', resolve);
      });
    });
  }

  return { write };
};