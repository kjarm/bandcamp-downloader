'use strict';

const assert = require('assert');
const createWriter = require('./writer');

describe('Writer', () => {
  it('should be able to write a song to the disk', async () => {
    // given
    const expectedTitle = 'inhale-radiation-fumes';
    const expectedDestination = '/home/user/songs';
    const expectedUrl = 'https://t4.bcbits.com/stream/44228e9894d4619b669a8d28620137e2/mp3-128/937624793';
    const https = createHttps();
    const fs = createFs();
    const writer = createWriter(https, fs);

    // when
    await writer.write({
      normalizedTitle: expectedTitle,
      url: expectedUrl
    }, expectedDestination);

    // then
    assert.deepEqual(https.lastPipedTo, fs.lastStream);
    assert.equal(fs.lastStream.filePath, `${expectedDestination}/${expectedTitle}.mp3`);
    assert.equal(https.lastCall.url, expectedUrl);
  });
});

function createHttps() {
  let lastPipedTo = {};
  let lastCall = {};
  function get(url, callback) {
    lastCall.url = url;
    const res = {
      pipe(stream) {
        Object.assign(lastPipedTo, stream);
      },
      on() {

      }
    };
    callback(res);
  }

  return {
    lastPipedTo,
    lastCall,
    get
  };
}

function createFs() {
  let lastStream = {};
  function createWriteStream(filePath) {
    Object.assign(lastStream, {
      filePath,
      on(event, callback) {
        if(event === 'finish') {
          process.nextTick(callback);
        }
      }
    });
    return lastStream;
  }

  return { 
    lastStream,
    createWriteStream
  };
}