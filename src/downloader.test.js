const createDownloader = require('./downloader');
const assert = require('assert');

describe('Downloader', () => {
  it('should download a song based on the album url', async () => {
    // given
    const expectedAlbumUrl = 'https://blackholeprods.bandcamp.com/album/deformed-by-law';
    const expectedTitle = 'sadistic-executioner';
    const expectedUrl = 'https://t4.bcbits.com/stream/09d7dfe8a4631a8045bd702995c20f01/(...)';
    const expectedDestination = '/home/user/songs';
    const expectedOperation = {
      result: 'ok'
    };
    const crawler = {
      async getSong(albumUrl) {
        if (albumUrl === expectedAlbumUrl) {
          return {
            normalizedTitle: expectedTitle,
            url: expectedUrl
          };
        }
        throw new Error(`Wrong argument: ${albumUrl}`);
      }
    };
    const writer = {
      async write(song, destination) {
        if (song.normalizedTitle === expectedTitle &&
        song.url === expectedUrl &&
        destination === expectedDestination) {
          return expectedOperation;
        }
        throw new Error(`Wrong arguments: ${JSON.stringify(song)}, ${destination}`);
      }
    };
    const downloader = createDownloader(crawler, writer);
    
    // when
    const operation = await downloader.download(expectedAlbumUrl, expectedDestination);

    // then
    assert.deepEqual(operation, expectedOperation);
  });
});
