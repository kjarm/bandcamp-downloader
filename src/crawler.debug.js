const puppeteer = require('puppeteer');
const createCrawler = require('./crawler');

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  const crawler = createCrawler(page);
  const song = await crawler.getSong('https://blackholeprods.bandcamp.com/album/deformed-by-law');
  console.log(song);

  await browser.close();
})();