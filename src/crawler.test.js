'use strict';

const assert = require('assert');
const createCrawler = require('./crawler');

describe('Crawler', () => {
  it('should be able to get song from an album', async () => {
    // given
    const expectedAlbumUrl = 'https://blackholeprods.bandcamp.com/album/deformed-by-law';
    const expectedTitle = 'sadistic-executioner';
    const expectedUrl = 'https://t4.bcbits.com/stream/09d7dfe8a4631a8045bd702995c20f01/(...)';
    const expectedSong = {
      normalizedTitle: expectedTitle,
      url: expectedUrl
    };
    const document = createHappyPathDocument();
    const page = createPage();
    const crawler = createCrawler(page, document);

    // when
    const song = await crawler.getSong(expectedAlbumUrl);

    // then
    assert.deepEqual(song, expectedSong);
    assert.deepEqual(page.interactions, [`went to ${expectedAlbumUrl}`]);
    assert.deepEqual(document.interactions, [
      'play clicked',
      'audio attribute src checked',
      'audio attribute src obtained',
      'title link attribute href obtained'
    ]);
  });
});

function createPage() {
  const interactions = [];
  function goto(url) {
    interactions.push(`went to ${url}`);
  }
  async function evaluate(func) {
    return func();
  }
  async function waitForFunction(func) {
    return func();
  }
  
  return {
    interactions,
    goto,
    evaluate,
    waitForFunction
  };
}

function createHappyPathDocument() {
  const interactions = [];
  function querySelector(selector) {
    if (selector === '.playbutton') {
      return {
        click() {
          interactions.push('play clicked');
        }
      };
    }
    if (selector === 'audio') {
      return {
        hasAttribute(name) {
          interactions.push(`audio attribute ${name} checked`);
        },
        getAttribute(name) {
          interactions.push(`audio attribute ${name} obtained`);
          return 'https://t4.bcbits.com/stream/09d7dfe8a4631a8045bd702995c20f01/(...)';
        }
      };
    }
    if (selector === '.title_link') {
      return {
        getAttribute(name) {
          interactions.push(`title link attribute ${name} obtained`);
          return '/track/sadistic-executioner';
        }
      };
    }
  }

  return {
    interactions,
    querySelector
  };
};
