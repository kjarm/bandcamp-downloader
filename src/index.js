'use strict';

const https = require('https');
const fs = require('fs');
const puppeteer = require('puppeteer');
const createCrawler = require('./crawler');
const createWriter = require('./writer');
const createDownloader = require('./downloader');

(async () => {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();

  const crawler = createCrawler(page);
  const writer = createWriter(https, fs);
  const downloader = createDownloader(crawler, writer);

  await downloader.download('https://blackholeprods.bandcamp.com/album/deformed-by-law', process.cwd());
  console.log('Success!');
  await browser.close();
})();