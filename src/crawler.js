'use strict';

module.exports = function createCrawler(page, document) {
  async function getSong(albumUrl) {
    await page.goto(albumUrl);
    await page.evaluate(() => {
      document.querySelector('.playbutton').click();
    });
    await page.waitForFunction(() => {
      return document.querySelector('audio').hasAttribute('src');
    });
    const url = await page.evaluate(() => {
      return document.querySelector('audio').getAttribute('src');
    });
    const normalizedTitle = await page.evaluate(() => {
      return document.querySelector('.title_link').getAttribute('href').replace('/track/', '');
    });

    return { url, normalizedTitle };
  }
  return { getSong };
}