'use strict';

const https = require('https');
const fs = require('fs');
const createWriter = require('./writer');

(async () => {
  const writer = createWriter(https, fs);
  await writer.write({
    normalizedTitle: 'inhale-radiation-fumes',
    url: 'https://t4.bcbits.com/stream/44228e9894d4619b669a8d28620137e2/mp3-128/937624793?p=0&ts=1529744155&t=c3bda677950f3cc3c78a2b1e29d4b7c71f9300af&token=1529744155_e89245ba48cc87317ac98c81c71cbc68a8207eae'
  }, __dirname);
})();